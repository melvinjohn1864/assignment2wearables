//
//  ShapeViewController.swift
//  Assignment2
//
//  Created by Melvin John on 2019-11-05.
//  Copyright © 2019 Melvin John. All rights reserved.
//

import Foundation
import UIKit
import Particle_SDK

class ShapeViewController: UIViewController {
    
    let USERNAME = "melvinjohn1864@gmail.com"
    let PASSWORD = "Melvin@1864"
    
    let DEVICE_ID = "25003d000447363333343435"
    var myPhoton : ParticleDevice?
    
    var imageName: String!
    
    var angle: Float!
    
    @IBOutlet weak var rotatingShape: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageName = "rectangle"
        self.rotatingShape.image = UIImage(named: imageName)
        
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
        
    }
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                
                self.subscribeToParticleEvents()
                //self.subscribeToXMotion()
            }
            
        }
    }
    
    
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
                    //let angle:Int = Int(choice)!
                    if (choice == "A") {
                        
                    }else if (choice == "B") {
                        
                    }else if (choice == "C") {
                        
                    }else {
                        
                    print(choice)
                    self.angle = (choice as NSString).floatValue
                    self.rotateImage(angle: self.angle)
                    }
                    
                }
        })
    }
    
    
    func generateRandomImages()->String{
        
        let shapes: [String] = ["rectangle", "triangle"]
        
        return shapes[Int.random(in: 0 ..< shapes.count)]
        
    }
    
    func rotateImage(angle: Float){
        DispatchQueue.main.async {
            self.rotatingShape.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
        }
    }
}
