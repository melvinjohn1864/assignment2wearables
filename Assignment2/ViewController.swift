//
//  ViewController.swift
//  Assignment2
//
//  Created by Melvin John on 2019-11-04.
//  Copyright © 2019 Melvin John. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    // MARK: User variables
    let USERNAME = "melvinjohn1864@gmail.com"
    let PASSWORD = "Melvin@1864"
    
    let DEVICE_ID = "25003d000447363333343435"
    var myPhoton : ParticleDevice?
    
    var imageName: String!
    
    var originalXPos: CGFloat!
    var originalYPos: CGFloat!
    var angle: Float!

    @IBOutlet weak var nextQues: UIButton!
    @IBOutlet weak var shape: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        imageName = "rectangle"
        self.shape.image = UIImage(named: imageName)
        
        originalXPos = shape.frame.minX
        originalYPos = shape.frame.minY
        
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
    }
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                
                self.subscribeToParticleEvents()
                //self.subscribeToXMotion()
            }
            
        }
    }
    
    
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
    
                    if (choice == "A") {
                        if self.imageName == "rectangle"{
                            self.turnParticleRed()
                        }else if self.imageName == "triangle" {
                            self.turnParticleRed()
                        }
                    }else if (choice == "B") {
                        if self.imageName == "rectangle"{
                            self.turnParticleRed()
                        }else if self.imageName == "triangle" {
                            self.turnParticleGreen()
                        }
                    }else if (choice == "C") {
                        if self.imageName == "rectangle"{
                            self.turnParticleGreen()
                        }else if self.imageName == "triangle" {
                            self.turnParticleRed()
                        }
                    }else {
                        //let angle:Int = Int(choice)!
                        print(choice)
                        self.angle = (choice as NSString).floatValue
                        self.rotateImage(angle: self.angle)
                    
                    }
                }
        })
    }
    

    
    
    func turnParticleGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = ["green"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        
    }
    
    func turnParticleRed() {
        
        print("Pressed the change lights button")
        
        let parameters = ["red"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        
    }
    
    func rotateImage(angle: Float){
        DispatchQueue.main.async {
            self.shape.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
        }
    }
    
    
    

    @IBAction func moveToNextQues(_ sender: Any) {
        imageName = "triangle"
        self.shape.image = UIImage(named: imageName)
    }
    
}

