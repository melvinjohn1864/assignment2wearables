//
//  LaunchViewController.swift
//  Assignment2
//
//  Created by Melvin John on 2019-11-04.
//  Copyright © 2019 Melvin John. All rights reserved.
//

import Foundation
import UIKit

class LaunchViewController: UIViewController {
    
    @IBOutlet weak var nextScreen: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
