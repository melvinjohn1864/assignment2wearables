// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include <math.h>

InternetButton button = InternetButton();

void setup() {
button.begin();

Particle.function("answer", showCorrectOrIncorrect);

}

int showCorrectOrIncorrect(String cmd){
    if (cmd == "green"){
        button.allLedsOn(0,255,0);
        delay(2000);
        button.allLedsOff();
    }else if (cmd == "red"){
        button.allLedsOn(255,0,0);
        delay(2000);
        button.allLedsOff();
    }else {
        return -1;
    }
    return 1;
}
int DELAY = 200;

char result[10];

void loop() {

  
  if (button.buttonOn(1)) {
    
    Particle.publish("playerChoice",result, 60, PRIVATE);
  }
  if (button.buttonOn(2)) {
    // CHOICE = B
    Particle.publish("playerChoice","A", 60, PRIVATE);
    delay(DELAY);
  }

  if (button.buttonOn(3)) {
      // CHOICE = C
    Particle.publish("playerChoice","B", 60, PRIVATE);
    delay(DELAY);
  }
  
  if (button.buttonOn(4)) {
      // CHOICE = C
    Particle.publish("playerChoice","C", 60, PRIVATE);
    delay(DELAY);
  }
  
    int xPos = button.readX();
    int yPos = button.readY();
    int zPos = button.readZ();
    
    
    int Rad2Deg = 180.0 / M_PI;
    
    int rotAngle = atan2(yPos - 0, xPos - 0) * Rad2Deg;
    
    sprintf(result, "%d", abs(rotAngle)); 
  
}